//
//  ViewController.swift
//  TestCalendar
//
//  Created by Click Labs 65 on 3/3/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

  var permanentCurrentWeekArray : [NSDate] = []
  var currentWeekArray : [NSDate] = []
  var nextWeekArray :[NSDate] = []
 var previousWeekArray :[NSDate] = []
  var currentMonthArray : [String] = []
  var nextMonthArray : [String] = []
  var previousMonthArray : [String] = []
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    
    //nextDateCalculations()
  //  today()
   // currentWeek()
   // nextWeek()
   // nextWeek()
   // previousWeek()
    
    currentMonth()
    
    nextMonth()
    
    previousMonth()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  
  func today() {
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    let components = calendar.components(.MonthCalendarUnit | .DayCalendarUnit | .YearCalendarUnit | .CalendarUnitWeekOfMonth | .CalendarUnitWeekday , fromDate: date)
    println(components)
    println(calendar.firstWeekday)
    
    var dateFormatter = NSDateFormatter()
    
    dateFormatter.dateFormat = "EEEE"
    
    var dayName = dateFormatter.stringFromDate(date)
    println(dayName)

    
    
    
  }
  
  

  func nextDateCalculations() {
    
    //implementing next week
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    
    let components = NSDateComponents()
    components.weekOfYear = 1
    components.hour = 12
    
    println("1 week and 12 hours from now: \(calendar.dateByAddingComponents(components, toDate: date, options: nil)!)")

    let newComponents = NSDateComponents()
    newComponents.day = 1
    println("NextDay = \(calendar.dateByAddingComponents(newComponents, toDate: date, options: nil)!)")
    
    newComponents.day = -1
    println("PreviousDay = \(calendar.dateByAddingComponents(newComponents, toDate: date, options: nil)!)")
  
  }
  
  
  func currentWeek(){
    
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
 
    var dateComponent = NSDateComponents()
    dateFormatter.dateFormat = "EEEE"
    
    var dayName = dateFormatter.stringFromDate(date)  //day name contains current day name
    
    var weeksSunday = ""
    
    if dayName == "Sunday" {
      
      weeksSunday = dateFormatter.stringFromDate(date)
      
    } else if dayName == "Monday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = 0
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
      
    } else if dayName == "Tuesday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -1
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
 
    } else if dayName == "Wednesday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -2
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)

    } else if dayName == "Thursday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -3
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)

      
    } else if dayName == "Friday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -4
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)

    } else if dayName == "Saturday" {
      dateFormatter.dateFormat = "yyyy-MM-dd"
      dateComponent.day = -5
      var newDate : NSDate =  calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
      weeksSunday = dateFormatter.stringFromDate(newDate)
    }
    
    
    var i = 0
    var newSunday : NSDate = dateFormatter.dateFromString(weeksSunday)!
    currentWeekArray.append(newSunday)
    
    println(newSunday)
    for i in 0...5 {
      dateComponent.day = 1
      newSunday = calendar.dateByAddingComponents(dateComponent, toDate: newSunday, options: nil)!
      println(newSunday)
      currentWeekArray.append(newSunday)
      }
    
    println(currentWeekArray)
  }
  
  //when value is converted to date date and hours are implicitely added
  

  func nextWeek(){
    
    let calendar = NSCalendar.currentCalendar()
    var currentSundayDate : NSDate = currentWeekArray[0]
    var dateComponent = NSDateComponents()
    dateComponent.day = 7
    var newSundayDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: currentSundayDate, options: nil)!
   println("\nnext week\n\(newSundayDate)")
    
    nextWeekArray.append(newSundayDate)
    
    for i in 0...5 {
      dateComponent.day = 1
      newSundayDate = calendar.dateByAddingComponents(dateComponent, toDate: newSundayDate, options: nil)!
      println(newSundayDate)
      nextWeekArray.append(newSundayDate)
    }
    
    println(nextWeekArray)
    currentWeekArray = nextWeekArray
  }

  
  

func previousWeek(){
  
  let calendar = NSCalendar.currentCalendar()
  var currentSundayDate : NSDate = currentWeekArray[0]
  var dateComponent = NSDateComponents()
  dateComponent.day = -7
  var newSundayDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: currentSundayDate, options: nil)!
  
  let components = calendar.components(.MonthCalendarUnit , fromDate: newSundayDate)
  println("\(components)\n\n")
  
  println("\nprevious week\n\(newSundayDate)")



  previousWeekArray.append(newSundayDate)
  
  for i in 0...5 {
    dateComponent.day = 1
    newSundayDate = calendar.dateByAddingComponents(dateComponent, toDate: newSundayDate, options: nil)!
    println(newSundayDate)
    previousWeekArray.append(newSundayDate)
  }
  
  println(previousWeekArray)
  currentWeekArray = previousWeekArray
  }

//January,March,may,july,august,october,december - 31 days feb if leap 29 else 28 and April june september november have 30 days


  func currentMonth(){
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
    
    var dateComponent = NSDateComponents()
    dateFormatter.dateFormat = "MMMM"
    
    var monthName = dateFormatter.stringFromDate(date)  //day name contains current month name
    dateFormatter.dateFormat = "dd"
    var currentDate = dateFormatter.stringFromDate(date)
    println("Current date \(currentDate)")
    println("Current month \(monthName)")
    
    var decrementor = (currentDate.toInt()!) - 1
    decrementor = -decrementor
    dateComponent.day = decrementor
    var newDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
    dateFormatter.dateFormat = "EEEE"
    var firstDay = dateFormatter.stringFromDate(newDate)
    println("First Day was on \(firstDay)")
    let components = calendar.components(.MonthCalendarUnit | .YearCalendarUnit , fromDate: newDate)
    var leap = components.leapMonth
println("\n\nCurrent year \(components.year)")
    
    if monthName == "January" ||  monthName == "March" ||  monthName == "May" ||  monthName == "July" ||  monthName == "August" ||  monthName == "October" ||  monthName == "December" {
      
      for i in 1...31 {
        currentMonthArray.append(String(i))
      }
    } else if monthName == "February"  && leap == true {
      for i in 1...28 {
        currentMonthArray.append(String(i))
      }
    } else if monthName == "February" && leap == false {
      for i in 1...29 {
        currentMonthArray.append(String(i))
      }
    } else {
      for i in 1...30 {
        currentMonthArray.append(String(i))
        
      }
      
    }
    
println(currentMonthArray)

  }



  func nextMonth(){
    
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
   
    
    var dateComponent = NSDateComponents()
    dateComponent.month = 1
     var nextMonthDate = calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
    println(nextMonthDate)
    
    
    dateFormatter.dateFormat = "MMMM"
    
    var monthName = dateFormatter.stringFromDate(nextMonthDate)  //day name contains current month name
    dateFormatter.dateFormat = "dd"
    var currentDate = dateFormatter.stringFromDate(nextMonthDate)
    println("\nnextMonthDate  \(currentDate)")
    println("nextMonth \(monthName)")
    
    var decrementor = (currentDate.toInt()!) - 1
    decrementor = -decrementor
    dateComponent.day = decrementor
    
    dateComponent.month = 0
    var newDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: nextMonthDate, options: nil)!
    println(newDate)
    dateFormatter.dateFormat = "EEEE"
    var firstDay = dateFormatter.stringFromDate(newDate)
    println("First Day is on \(firstDay)")
    let components = calendar.components(.MonthCalendarUnit , fromDate: newDate)
    var leap = components.leapMonth
    
    
    if monthName == "January" ||  monthName == "March" ||  monthName == "May" ||  monthName == "July" ||  monthName == "August" ||  monthName == "October" ||  monthName == "December" {
      
      for i in 1...31 {
        nextMonthArray.append(String(i))
      }
    } else if monthName == "February"  && leap == true {
      for i in 1...28 {
        nextMonthArray.append(String(i))
      }
    } else if monthName == "February" && leap == false {
      for i in 1...29 {
        nextMonthArray.append(String(i))
      }
    } else {
      for i in 1...30 {
        nextMonthArray.append(String(i))
        
      }
      
    }
println(nextMonthArray)
    
  }


  func previousMonth(){
    
    
    let calendar = NSCalendar.currentCalendar()
    let date = NSDate()
    var dateFormatter = NSDateFormatter()
    
    
    var dateComponent = NSDateComponents()
    dateComponent.month = -1
    var previousMonthDate = calendar.dateByAddingComponents(dateComponent, toDate: date, options: nil)!
    println(previousMonthDate)
    
    
    dateFormatter.dateFormat = "MMMM"
    
    var monthName = dateFormatter.stringFromDate(previousMonthDate)  //day name contains current month name
    dateFormatter.dateFormat = "dd"
    var currentDate = dateFormatter.stringFromDate(previousMonthDate)
    println("\npreviousMonthDate  \(currentDate)")
    println("previousMonth \(monthName)")
    
    var decrementor = (currentDate.toInt()!) - 1
    decrementor = -decrementor
    dateComponent.day = decrementor
    
    dateComponent.month = 0
    var newDate : NSDate = calendar.dateByAddingComponents(dateComponent, toDate: previousMonthDate, options: nil)!
    println(newDate)
    dateFormatter.dateFormat = "EEEE"
    var firstDay = dateFormatter.stringFromDate(newDate)
    println("First Day is on \(firstDay)")
    let components = calendar.components(.MonthCalendarUnit , fromDate: newDate)
    var leap = components.leapMonth
    
    
    if monthName == "January" ||  monthName == "March" ||  monthName == "May" ||  monthName == "July" ||  monthName == "August" ||  monthName == "October" ||  monthName == "December" {
      
      for i in 1...31 {
       previousMonthArray.append(String(i))
      }
    } else if monthName == "February"  && leap == false {
      for i in 1...28 {
        previousMonthArray.append(String(i))
      }
    } else if monthName == "February" && leap == true {
      for i in 1...29 {
        previousMonthArray.append(String(i))
      }
    } else {
      for i in 1...30 {
        previousMonthArray.append(String(i))
        
      }
      
    }
    println(previousMonthArray)
    

    
    
  }








}
















